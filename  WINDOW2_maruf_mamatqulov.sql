WITH yearly_sales AS (
  SELECT
    c.name AS prod_subcategory,
    EXTRACT(YEAR FROM r.rental_date) AS sales_year,
    SUM(p.amount) AS total_sales
  FROM
    rental r
    JOIN payment p ON r.rental_id = p.rental_id
    JOIN inventory i ON r.inventory_id = i.inventory_id
    JOIN film f ON i.film_id = f.film_id
    JOIN film_category fc ON f.film_id = fc.film_id
    JOIN category c ON fc.category_id = c.category_id
  WHERE
    EXTRACT(YEAR FROM r.rental_date) >= 1998 AND EXTRACT(YEAR FROM r.rental_date) <= 2001
  GROUP BY
    c.name,
    EXTRACT(YEAR FROM r.rental_date)
),
previous_year_sales AS (
  SELECT
    prod_subcategory,
    sales_year,
    total_sales,
    LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY sales_year) AS previous_year_sales
  FROM
    yearly_sales
)
SELECT DISTINCT
  prod_subcategory
FROM
  previous_year_sales
WHERE
  total_sales > previous_year_sales;